using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class mov : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    private Transform _transform;
    //Constantes
    private const String TAG_ENEMY = "Enemigo";
    //Propiedades
    private bool isIntangible = false;
    private float intangibleTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        _transform = GetComponent<Transform>();


    }

    // Update is called once per frame
    void Update()
    {
     
        rb.velocity = new Vector2(5,0);


        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(10, rb.velocity.y);
            animator.SetInteger("Estado", 0);
            sr.flipX = false;
        }
        
        if (Input.GetKey(KeyCode.X))
        {
            animator.SetInteger("Estado", 1);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(new Vector2(0, 10), ForceMode2D.Impulse);
            return ;
            
            animator.SetInteger("Estado", 2);
          

        }


        if (Input.GetKey(KeyCode.Z))
        {
            animator.SetInteger("Estado", 3);
        }
        
        if (isIntangible && intangibleTime < 2f)
        {
            intangibleTime += Time.deltaTime;
            sr.enabled = !sr.enabled;
            Debug.Log(intangibleTime);
        }
        else if (isIntangible && intangibleTime >= 3f)
        {
            isIntangible = false;
            sr.enabled = true;
            intangibleTime = 0f;
          //  Physics2D.IgnoreCollision();
         // Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>(),false);

          
            
        }


    }
   
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag ==  "Ground")
        {
        

        Debug.Log("Collision" + collision.gameObject.name); //Con que elemento colosiono
    }

        if (collision.gameObject.tag ==  "Rompible")
        {
        

            Debug.Log("Destruir Caja " ); //Con que elemento colosiono
        }

        if (collision.gameObject.tag == "enemy")
        {
            transform.localScale = new Vector3(0.3f, 0.3f, 1);
            isIntangible = true;
        }

        if (collision.gameObject.name == "enemy")
        {
            Destroy(collision.gameObject);
           
        }


//        if (collision.gameObject.CompareTag(TAG_ENEMY))
     //   {
     //       transform.localScale = new Vector3(0.3f, 0.3f, 1);
           // isIntangible = true;
           // Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>(),);
    //    }

        if (collision.gameObject.name == "Vida")
        {
            transform.localScale = new Vector3(0.7f, 0.7f, 1);
            Destroy(collision.gameObject);
        }

        {
            
        }

       
    }

  //  private void OnTriggerEnter2D(Collider2D collider)// traspasar 
 //   {
 //       Debug.Log("Trigger");
  //  }
}







    
  

   
   
